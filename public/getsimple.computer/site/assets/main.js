function IsMobile() {
    // NOTE(matt): From https://medium.com/simplejs/detect-the-users-device-type-with-a-simple-javascript-check-4fc656b735e1
    var Identifier = navigator.userAgent||navigator.vendor||window.opera;
    var Result = (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(Identifier)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(Identifier.substr(0,4)));
    return Result;
};

// YouTube API
//
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
    //var VideoStyle = window.getComputedStyle(Video);
    var Dim = SizePremiere();
	player = new YT.Player('player', {
		height: Dim.Video.Y,
		width: Dim.Video.X,
		videoId: VideoID,
        playerVars: { "playsinline": "1" },
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
    Video.style.width = Dim.Video.X + "px";
    Video.style.height = Dim.Video.Y + "px";
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
	//event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
    console.log(event);
	if (event.data == YT.PlayerState.PLAYING && !done) {
		//setTimeout(stopVideo, 6000);
		done = true;
	}
}
//
////

function
GetTimestampFromMilliseconds(Milliseconds)
{
    var Result = {
        Days: 0,
        Hours: 0,
        Minutes: 0,
        Seconds: 0,
        Negative: false,
    };

    var RunningTime = Math.floor(Milliseconds / 1000);
    if(RunningTime < 0)
    {
        Result.Negative = true;
        RunningTime = -RunningTime;
    }

    Result.Seconds = RunningTime % 60;
    RunningTime -= Result.Seconds;
    RunningTime /= 60;
    Result.Minutes = RunningTime % 60;
    RunningTime -= Result.Minutes;
    RunningTime /= 60;
    Result.Hours = RunningTime % 24;
    RunningTime -= Result.Hours;
    RunningTime /= 24;
    Result.Days = RunningTime;

    return Result;
}

function
GetPrintableTimestamp(Timestamp)
{
    var Result = "";
    if(Timestamp.Negative)
    {
        Result += "-";
    }

    if(Timestamp.Days > 0)
    {
        Result += Timestamp.Days;
        Result += " Day";
        if(Timestamp.Days > 1)
        {
            Result += "s";
        }
        Result += ", ";
    }
    var DesiredDigits = 2;
    Result += Timestamp.Hours.toString().padStart(DesiredDigits, 0);
    Result += ":";
    Result += Timestamp.Minutes.toString().padStart(DesiredDigits, 0);
    Result += ":";
    Result += Timestamp.Seconds.toString().padStart(DesiredDigits, 0);
    return Result;
}

function
MinutesInTimestamp(Timestamp)
{
    var Result = 0;
    Result += Timestamp.Days * 60 * 60;
    Result += Timestamp.Hours * 60;
    Result += Timestamp.Minutes;
    if(Timestamp.Negative)
    {
        Result = -Result;
    }
    return Result;
}

function
RemoveElement(Element, Interval)
{
    Element.remove();
    clearInterval(Interval);
    Interval = null;
}

function
Shutter(Element)
{
    if(ActivateNoticeIntervalID)
    {
        var ElementStyle = window.getComputedStyle(Element);
        var Height = parseInt(ElementStyle.height);
        var PaddingTop = parseInt(ElementStyle.paddingTop);
        var PaddingBottom = parseInt(ElementStyle.paddingBottom);
        if(Height > 0 || PaddingTop > 0 || PaddingBottom > 0)
        {
            if(Height > 0)
            {
                Element.style.height = --Height + "px";
            }
            if(PaddingTop > 0)
            {
                Element.style.paddingTop = --PaddingTop + "px";
            }
            if(PaddingBottom > 0)
            {
                Element.style.paddingBottom = --PaddingBottom + "px";
            }
        }
        else
        {
            clearInterval(ActivateNoticeIntervalID);
            ActivateNoticeIntervalID = null;
            Element.remove();
            Element = null;
        }
    }
}

function
RemoveElementShutter(Element, Delay)
{
    if(Element && !ActivateNoticeIntervalID)
    {
        ActivateNoticeIntervalID = setInterval(Shutter, Delay / 100, Element);
    }
}

function
RemoveElementFade(Element){
    // NOTE(matt): Credit to https://chrisbuttery.com/articles/fade-in-fade-out-with-javascript/
    Element.style.opacity = 1;
    (function fade() {
        if ((Element.style.opacity -= .02) < 0) {
            Element.remove();
        } else {
            requestAnimationFrame(fade);
        }
    })();
}

function
UpdateCountdown()
{
    var Now = Date.now();
    var TimeDiff = GetTimestampFromMilliseconds(PremiereTime - Now);
    var MinsToGo = MinutesInTimestamp(TimeDiff);
    if(MinsToGo >= 2)
    {
        if(Activated === true && ActivateNotice && !TriggeredRemoval)
        {
            RemoveElementShutter(ActivateNotice, 2000);
            TriggeredRemoval = true;
        }
        var Content = "The pitch begins in " + GetPrintableTimestamp(TimeDiff);
        TimeToGo.textContent = Content;
    }
    else
    {
        if(Countdown)
        {
            player.playVideo();
            RemoveElementFade(Countdown);
            Countdown = null;
        }
        clearInterval(CountdownIntervalID);
    }
}

function
ParseTimeString(TimeString, MinuteOffset)
{
    var T = {
        Year: 0,
        Month: 0,
        Day: 0,
        Hour: 0,
        Minute: 0,
        Second: 0,
    };

    var ParsedDateTime = TimeString.split(" ");

    if(ParsedDateTime.length > 0)
    {
        var ParsedDate = ParsedDateTime[0].split("-");
        if(ParsedDate.length == 3)
        {
            T.Year = parseInt(ParsedDate[0]);
            T.Month = parseInt(ParsedDate[1]) - 1; // NOTE(matt): This one is 0-indexed
            T.Day = parseInt(ParsedDate[2]);
        }

        if(ParsedDateTime.length > 1)
        {
            var ParsedTime = ParsedDateTime[1].split(":");
            if(ParsedTime.length == 2)
            {
                T.Hour = parseInt(ParsedTime[0]);
                T.Minute = parseInt(ParsedTime[1]) + MinuteOffset;
            }
        }
    }
   
    var Result = new Date(Date.UTC(T.Year, T.Month, T.Day, T.Hour, T.Minute, T.Second));
    return Result;
}

function
SizePremiere()
{
    var VideoDim = {
        X: 0,
        Y: 0,
    };

    var ChatDim = {
        X: 0,
        Y: 0,
    };

    var Result = {
        Video: VideoDim,
        Chat: ChatDim,
    };

    var MaxX = window.innerWidth;
    var MaxY = window.innerHeight;

    var PremiereStyle = window.getComputedStyle(Premiere);
    var PaddingTop    = parseInt(PremiereStyle.paddingTop);
    var PaddingRight  = parseInt(PremiereStyle.paddingRight);
    var PaddingBottom = parseInt(PremiereStyle.paddingBottom);
    var PaddingLeft   = parseInt(PremiereStyle.paddingLeft);

    var PaddingVertical = PaddingTop + PaddingBottom;
    var PaddingHorizontal = PaddingLeft + PaddingRight;

    MaxX -= PaddingHorizontal;
    MaxY -= PaddingVertical;

    var WiggleRoom = 32;
    MaxX -= WiggleRoom;
    MaxY -= WiggleRoom;

    if(Chat)
    {
        Result.Chat.X = Math.min(560, MaxX * .75);
        MaxX -= Result.Chat.X;
    }

    var DimYFromMaxX = Math.floor(MaxX * 9 / 16);
    var DimXFromMaxY = Math.floor(MaxY * 16 / 9);

    if(DimXFromMaxY > MaxX)
    {
        Result.Video.X = Math.floor(MaxX);
        Result.Video.Y = Math.floor(DimYFromMaxX);
    }
    else if(DimYFromMaxX > MaxY)
    {
        Result.Video.Y = Math.floor(MaxY);
        Result.Video.X = Math.floor(DimXFromMaxY);
    }
    else
    {
        Result.Video.X = Math.floor(MaxX);
        Result.Video.Y = Math.floor(DimYFromMaxX);
    }

    Result.Chat.Y = Result.Video.Y;

    return Result;
}

var Premiere = document.getElementById("premiere");
var Video = Premiere.querySelector(".video");
var VideoID = Premiere.getAttribute("data-id");
// NOTE(matt):  We offset by 2-mins to account for YouTube's 2-minute countdown
var PremiereTime = ParseTimeString(Premiere.getAttribute("data-time"), 2);

var YoutubeVideo = document.getElementById("player");
var VideoURL = "https://www.youtube-nocookie.com/embed/" + VideoID;

var Now = Date.now();
var TimeDiff = GetTimestampFromMilliseconds(PremiereTime - Now);
var MinsToGo = MinutesInTimestamp(TimeDiff);

var Countdown = null;
var Chat = null;
var ActivateNotice = null;
var ActivateNoticeIntervalID = null;
var TriggeredRemoval = false;
var TimeToGo = null;

if(MinsToGo >= 2)
{
    var CountdownContainer = document.createElement("div");
    CountdownContainer.classList.add("countdown");

    var ActivateNoticeContainer = document.createElement("div");
    ActivateNoticeContainer.classList.add("activate-notice");
    ActivateNotice = CountdownContainer.appendChild(ActivateNoticeContainer);
    ActivateNotice.textContent = "Please interact with the page to allow the pitch to start on time";

    var TimeToGoElement = document.createElement("div");
    TimeToGoElement.classList.add("time-to-go");
    TimeToGo = CountdownContainer.appendChild(TimeToGoElement);
    var Content = "The pitch begins in " + GetPrintableTimestamp(TimeDiff);
    TimeToGo.textContent = Content;

    Countdown = Video.insertBefore(CountdownContainer, YoutubeVideo);
}

var Activated = false;

document.addEventListener("click", function() {
    Activated = true;
});

Premiere.addEventListener("click", function() {
    Activated = true;
});

if(!IsMobile() && !TimeDiff.Negative)
{
    var ChatFrame = document.createElement("iframe");
    ChatFrame.classList.add("chat");
    ChatFrame.setAttribute("allowfullscreen", "");
    ChatFrame.setAttribute("frameborder", "0");
    Chat = Premiere.appendChild(ChatFrame);
}

var Dim = SizePremiere();
if(Chat)
{
    var Domain = "getsimple.computer";
    var SrcURL = "https://www.youtube.com/live_chat?v=" + VideoID + "&embed_domain=" + Domain;
    Chat.setAttribute("src", SrcURL);
    Chat.setAttribute("width", Dim.Chat.X);
    Chat.setAttribute("height", Dim.Chat.Y);
}

Video.style.width = Dim.Video.X + "px";
Video.style.height = Dim.Video.Y + "px";

var CountdownIntervalID = setInterval(UpdateCountdown, 1000);
