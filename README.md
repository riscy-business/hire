# Hiring Page
## Codebase Notes
@NOTE: Slowly migrating away from vibe.d. Eventually I want a really simple D or C backend with a Caddy frontend.

## Maintenance Notes
### Build
* `dub build --build=release`
### Upgrade
0. `cd ~`
1. `systemctl stop hire`
2. `mv hire/hire hire/hire.bak`
3. `scp -i .build_server.pem $BUILD_USER@$BUILD_IP:hire/hire hire/hire`
4. `systemctl start hire`
### Monitor Logs
* `journalctl -ef --unit=hire --unit=caddy --no-hostname`
### Sendmail
* `ps aux | grep sendmail`
* `netstat -aln | grep 25`
* `sudo service sendmail {start,stop,restart,status}`
* `sudo vim /etc/{hosts,mail/sendmail.mc}`
* `sudo sendmailconfig`
#### Caddy
* needs access to ~/.local and ~/.config/caddy
* uses port 2019 for admin endpoint
    * `sudo lsof -i -P -n | grep LISTEN`
* `sudo setcap cap_net_bind_service=+ep ./caddy`
## My setup (for anyone wondering)
* I keep a local copy of the codebase on my laptop
* I use an Amazon EC2 t2.medium instance running Ubuntu 18.04 for a build server
* I use an Amazon EC2 t2.micro instance running Ubuntu 18.04 for production
