//@BUG: regression in dmd and/or phobos; I haven't hunted down when it happened,
//    : but RedBlackTree constructed at compile-time is broken now PepeHands
shared static this ()
{
    countries = import ("countries.txt")
        .lineSplitter
        .array
        .redBlackTree;
}
enum host = "127.0.0.1";
enum domain = "https://hire.riscy.tv";
enum port = 8080;
debug
{
/* enum recaptcha_secret = "test recaptcha failure via bogus key"; */
enum recaptcha_secret = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe";
enum recaptcha_site = "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI";
enum csp_header = "Content-Security-Policy-Report-Only";
enum csp_report_uri = "report-uri /csp_report";
}
else
{
//@NOTE: can't iterate keys at compile-time because D sucks
enum recaptcha_secret = import ("recaptcha_secret").strip;
enum recaptcha_site = import ("recaptcha_site").strip;
enum csp_header = "Content-Security-Policy";
enum csp_report_uri = "";
}
immutable RedBlackTree!string countries;
immutable struct Vars
{
    int FULL_NAME_LEN = 2048;
    int NICKNAME_LEN = 2048;
    int PRONUNCIATION_LEN = 2048;
    int ADDRESS_LEN = 512;
    int COUNTRY_LEN = 32;
    int EMAIL_LEN = 320;
    int HOURLY_RATE_LEN = 5;
    double HOURLY_RATE_MIN = 23.00;
    double HOURLY_RATE_MAX = 53.00;
    int BILLING_CYCLE_LEN = 7;
    int PROJECT_NAME_LEN = 128;
    int PROJECT_DESCRIPTION_LEN = 4096;
}
Vars vars;
void main()
{
    version (linux)
    {
    import etc.linux.memoryerror;
    static if (is (typeof (registerMemoryErrorHandler)))
        registerMemoryErrorHandler();
    }
    auto httpSettings = new HTTPServerSettings;
    httpSettings.hostName = host;
    httpSettings.port = port;
    debug httpSettings.bindAddresses = ["::1", "127.0.0.1"];
    auto httpRouter = new URLRouter;
    httpRouter
        .get("/", &index)
        .post("/verify", &verify)
        .get("*", serveStaticFiles("public/"));
    auto httpListener = listenHTTP(httpSettings, httpRouter);
    scope (exit) httpListener.stopListening();
    runApplication();
}
void index(HTTPServerRequest req, HTTPServerResponse res)
{
    auto styleNonce = newNonce();
    auto recaptchaNonce = newNonce();
    res.headers[csp_header] = "default-src 'none'; style-src 'nonce-%s'; script-src 'nonce-%s'; img-src 'self' www.gstatic.com; frame-src www.recaptcha.net; object-src 'none'; frame-ancestors 'none'; base-uri 'none'; form-action 'self'; %s"
            .format(styleNonce, recaptchaNonce, csp_report_uri);
    auto key = recaptcha_site;
    res.render!("index.dt", req, key, vars, countries, domain, styleNonce, recaptchaNonce, ValidationErrorData);
}
void verify(HTTPServerRequest req, HTTPServerResponse res)
{
    bool human = false;
    auto styleNonce = newNonce();
    @path ("/recaptcha/api/")
    interface IreCAPTCHA { Json getSiteverify(string secret, string response); }
    auto recaptcha = new RestInterfaceClient!IreCAPTCHA("https://www.google.com/");
    auto error = ValidationErrorData.init;
    string current_field;
    string[string] form;
    foreach (k,v; req.form.byKeyValue)
        form[k] = v.dup;
    current_field = "form_full_name";
    if (!sanitizeFormField!(Vars.FULL_NAME_LEN, c => !c.isControl)
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    current_field = "form_nickname";
    if (!sanitizeFormField!(Vars.NICKNAME_LEN, c => !c.isControl)
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    current_field = "form_pronunciation";
    if (!sanitizeFormField!(Vars.PRONUNCIATION_LEN, c => !c.isControl,
                Yes.optional)
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    current_field = "form_address";
    if (!sanitizeFormField!(Vars.ADDRESS_LEN, c => true)
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    current_field = "form_country";
    if(!sanitizeFormField!(Vars.COUNTRY_LEN, c => !c.isControl)
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    else if (form[current_field] !in countries)
    {
        error.msg = "Please select a country from the drop-down menu.";
        goto FORM_ERROR;
    }
    current_field = "form_email";
    try
    {
        form[current_field].validateEmail(vars.EMAIL_LEN);
    }
    catch (Exception e)
    {
        error.msg = e.msg;
        goto FORM_ERROR;
    }
    current_field = "form_hourly_rate";
    if (!sanitizeFormField!(Vars.HOURLY_RATE_LEN,
                c => c.isDigit || c == '.')
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    else
    {
        try
        {
            auto d = form[current_field].to!double;
            if (d < vars.HOURLY_RATE_MIN || d > vars.HOURLY_RATE_MAX)
            {
                error.msg = format!"Must be between $%.2f and $%.2f."
                    (vars.HOURLY_RATE_MIN, vars.HOURLY_RATE_MAX);
                goto FORM_ERROR;
            }
        }
        catch (ConvException e)
        {
            error.msg = "Must be a valid number.";
            goto FORM_ERROR;
        }
    }
    current_field = "form_billing_cycle";
    if (!sanitizeFormField!(Vars.BILLING_CYCLE_LEN, c => !c.isControl)
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    else if (form[current_field] != "Weekly" && form[current_field] != "Monthly")
    {
        error.msg = "Please select a billing cycle from the drop-down menu.";
        goto FORM_ERROR;
    }
    current_field = "form_project_name";
    if (!sanitizeFormField!(Vars.PROJECT_NAME_LEN, c => !c.isControl)
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    current_field = "form_project_description";
    if (!sanitizeFormField!(Vars.PROJECT_DESCRIPTION_LEN, c => true)
            (&form[current_field], &error.msg))
        goto FORM_ERROR;
    human = recaptcha.getSiteverify(recaptcha_secret, req.form["g-recaptcha-response"])
        ["success"]
        .get!bool;
    if (!human)
    {
        error.msg = "The reCAPTCHA was not completed successfully.";
        goto RECAPTCHA_FAILURE;
    }
    else
    {
        auto email = new Mail;
        email.headers["Date"] = Clock.currTime(PosixTimeZone.getTimeZone("Asia/Manila")).toRFC822DateTimeString();
        email.headers["Sender"] = "Hiring Page Form <hiring@riscy.tv>";
        email.headers["From"] = "Hiring Page Form <hiring@riscy.tv>";
        email.headers["To"] = "Neo Ar <neo@getsimple.computer>";
        email.headers["Subject"] = "[NEW JOB]: %s".format(form["form_project_name"]);
        email.headers["Content-Type"] = "text/plain;charset=utf-8";
        email.bodyText =
        [
            "Full Name: %s",
            "Nickname: %s",
            "Pronunciation: %s",
            "Email: %s",
            "Hourly Rate: $%s",
            "Billing Cycle: %s",
            "Address:\n%s",
            "%s",
            "\n%s"
        ].join("\n")
        .format(form["form_full_name"],
                form["form_nickname"],
                form["form_pronunciation"],
                form["form_email"],
                form["form_hourly_rate"],
                form["form_billing_cycle"],
                form["form_address"],
                form["form_country"],
                form["form_project_description"]);
        try
        {
            auto smtpSettings = new SMTPClientSettings("localhost", 25);
            sendMail(smtpSettings, email);
        }
        catch (Exception e)
        {
            logInfo(e.msg);
        }
        res.headers[csp_header] = "default-src 'none'; style-src 'nonce-%s'; script-src 'none'; img-src 'self'; frame-src 'none'; object-src 'none'; frame-ancestors 'none'; base-uri 'none'; form-action 'none'; %s"
            .format(styleNonce, csp_report_uri);
        res.render!("submitted.dt", domain, styleNonce);
        return;
    }
FORM_ERROR:
    error.field = current_field;
RECAPTCHA_FAILURE:
    req.context["error"] = error;
    auto recaptchaNonce = newNonce();
    res.headers[csp_header] = "default-src 'none'; style-src 'nonce-%s'; script-src 'nonce-%s'; img-src 'self' www.gstatic.com; frame-src www.recaptcha.net; object-src 'none'; frame-ancestors 'none'; base-uri 'none'; form-action 'self'; %s"
            .format(styleNonce, recaptchaNonce, csp_report_uri);
    auto key = recaptcha_site;
    res.render!("index.dt", req, key, vars, countries, domain, styleNonce, recaptchaNonce, ValidationErrorData);
}
debug void cspReport(HTTPServerRequest req, HTTPServerResponse res)
{
    //@NOTE: HTTPServerOption.parseJsonBody didn't work and is marked deprecated because it sucks
    //@NOTE: Gave up trying to get the json from req.bodyReader because it sucks
    logInfo("CSP Report dropped, use Network tab of Chrome DevTools");
    //@NOTE: Not sure if this is how you are supposed to handle POST requests...
    res.writeBody("");
}
struct ValidationErrorData
{
    string msg;
    string field;
}
char[] newNonce() @safe
{
    static SHA1HashMixerRNG m_rng;
    static ubyte[16] m_nonce;
    if (!m_rng) m_rng = new SHA1HashMixerRNG;
    m_rng.read(m_nonce);
    return Base64.encode(m_nonce);
}
bool sanitizeFormField(int len, alias banned, Flag!"optional" optional = No.optional)
    (string* str, string* error)
{
    //@NOTE: A few sanity-checks to early-out before we sanitize...
    static if (!optional) {
    if (str.empty)
    {
        *error = "This field is required.";
        goto ILL_FORMED;
    } }
    if ((*str).count > len)
    {
        *error = len.format!"Must not be greater than %s characters.";
        goto ILL_FORMED;
    }
    *str = (*str)
        .representation
        .sanitizeUTF8
        .byDchar
        .filter!banned
        .array
        .to!string;
    //@NOTE: ...but we need to double-check it's not empty after sanitization
    static if (!optional) {
    if (str.empty)
    {
        *error = "This field is required.";
        goto ILL_FORMED;
    } }
    return true;
ILL_FORMED:
    return false;
}
import std.ascii : isDigit;
import std.algorithm : filter;
import std.array : array, empty, join;
import std.base64 : Base64;
import std.container : RedBlackTree, redBlackTree;
import std.conv : ConvException, to;
import std.datetime.systime : Clock;
import std.datetime.timezone : PosixTimeZone;
import std.format : format;
import std.string : strip, lineSplitter, representation;
import std.typecons : Flag, Yes, No;
import std.uni : isControl;
import std.utf : byDchar, count;
import vibe.core.core : runApplication;
import vibe.core.log : logInfo;
import vibe.crypto.cryptorand : SHA1HashMixerRNG;
import vibe.data.json : Json;
import vibe.http.fileserver : serveStaticFiles;
import vibe.http.router : URLRouter;
import vibe.http.server : HTTPServerRequest, HTTPServerResponse, HTTPServerSettings, listenHTTP, render;
import vibe.inet.message : toRFC822DateTimeString;
import vibe.mail.smtp : Mail, SMTPClientSettings, sendMail;
import vibe.utils.string : sanitizeUTF8;
import vibe.utils.validation : validateEmail;
import vibe.web.common : path;
import vibe.web.rest : RestInterfaceClient;
